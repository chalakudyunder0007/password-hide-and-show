 const passwordspan=document.getElementById('inputpassword')
 const minlengthspan=document.getElementById('minlength')
 const maxlengthspan=document.getElementById('maxlength')
 const checkboxspan=document.getElementById('inputcheckbox')
 const regularlengthspan=document.getElementById('regularlength')


 var regularExpression  = /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[!@#$%^&*])[A-Za-z\d!@#$%^&*]$/;
 
 passwordspan.addEventListener("keyup",verification)
 function verification(){
    const pass=passwordspan.value
    // minimum length
     if(pass.length<6){
         minlengthspan.innerHTML="&#x274c"
     }else {
        minlengthspan.innerHTML= "&#x2705"
     } 
    
    // maximum length
    if(pass.length>10){
        maxlengthspan.innerHTML= "&#x274c"
     }else{
        maxlengthspan.innerHTML="&#x2705"
    }

  // Character length
   
    if(regularExpression.test(passwordspan)){
         regularlengthspan.innerHTML="&#x274c;"
    }
    else{
        regularlengthspan.innerHTML="&#x2705"
    }
 }

 checkboxspan.addEventListener('change',hiddenpassword)
function hiddenpassword(){
    const ischecked=this.checked
    if(ischecked){
        passwordspan.type="text"
    }
        
else{
    passwordspan.type="password"
}}